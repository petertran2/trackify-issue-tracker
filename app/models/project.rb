class Project < ApplicationRecord
	validates :name, presence: true, uniqueness: { case_sensitive: false }
	STATUSES = %w(Development Test Production Obsolete)
	validates :status, inclusion: { in: STATUSES }
	
	has_many :issues, dependent: :destroy
	belongs_to :creator, primary_key: :id, foreign_key: :creator_id,
		class_name: :User
end
