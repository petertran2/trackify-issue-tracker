class IssuesController < ApplicationController
	
	before_action :authorized
	
	def new
		@issue = Issue.new
		@issue.reporter_id = current_user.id
	end
	
	def create
		@issue = Issue.new(issue_params)
		@issue.reporter_id = current_user.id
		if @issue.save
			redirect_to issue_path(@issue)
    else
    	flash[:errors] = @issue.errors.full_messages
      redirect_to new_issue_path
    end
	end
	
	def show
		@issue = Issue.find(params[:id])
		@comments = []
		@issue.comments.each do |comment|
			object = {}
			object['id'] = comment.id
			object['commenter'] = comment.commenter.name
			object['content'] = comment.content
			object['created_at'] = comment.created_at
			@comments << object
		end
		@new_comment = Comment.new
		@new_comment.issue_id = @issue.id
		@new_comment.commenter_id = current_user.id
	end
	
	def edit
    @issue = Issue.find(params[:id])
  end

  def update
    @issue = Issue.find(params[:id])
    if @issue.update_attributes(issue_params)
    	redirect_to issue_path(@issue)
    else
    	flash[:errors] = @issue.errors.full_messages
      redirect_to edit_issue_path(@issue)
    end
  end
	
	def destroy
		@issue = Issue.find(params[:id])
		if @issue.destroy
			redirect_to dashboard_path
		else
			flash[:errors] = @issue.errors.full_messages
			redirect_to issue_path(@issue)
		end
	end
	
	private
	
	def issue_params
		params.require(:issue).permit(:project_id, :reporter_id, :assignee_id,
			:priority, :status, :subject, :description, :due_date)
	end
	
end
