class AddCreatorIdToProjects < ActiveRecord::Migration[6.0]
  def change
  	add_column :projects, :creator_id, :integer
  	add_index :projects, :creator_id
  end
end
