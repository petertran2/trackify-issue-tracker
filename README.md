# Trackify Issue Tracker

![Trackify Demo](https://gitlab.com/petertran2/trackify-issue-tracker/-/raw/master/README_images/Peek%202020-02-10%2019-39.gif)

This web app is a Ruby on Rails app that documents issues and bugs to help people manage projects more effectively. It allows one to create an issue ticket that is shared with other members of the team to facilitate collaboration. Team members may comment on a ticket as they track their progress in resolving an issue. This app is inspired by Mantis Bug Tracker, a free and open-source web-based bug tracking system.

**DEMO:** https://trackify-issue-tracker.herokuapp.com/

You may use this account to try out the app:

    Username: demo@demo.com
    Password: demo

## Technologies

- Ruby
- Ruby on Rails
- PostgreSQL
- RSpec
- HTML
- CSS
- Javascript
- jQuery
- Vue.js
- Heroku
- bcrypt

### Author
Peter Tran